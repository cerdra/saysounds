package bob.SaySounds;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import net.minecraftforge.common.MinecraftForge;

import bob.SaySounds.misc.LibCommon;
import bob.SaySounds.proxy.CommonProxy;

@Mod(modid = LibCommon.MOD_ID, name = LibCommon.MOD_NAME, version = "0.1")
public class SaySounds
{

    @SidedProxy(serverSide = LibCommon.PROXY_COMMON, clientSide = LibCommon.PROXY_CLIENT)
    public static CommonProxy proxy;

    @Instance(LibCommon.MOD_ID)
    public static SaySounds INSTANCE;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        System.out.println("SaySounds loaded!");
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(new ModChatListener());
        FMLCommonHandler.instance().bus().register(new ModChatListener());
    }
}
