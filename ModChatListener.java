package bob.SaySounds;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.world.WorldEvent;

public class ModChatListener {

    net.minecraft.world.World theWorld;

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void listen(ServerChatEvent event) {
        String msg = event.message.toLowerCase().trim();
        if (msg.contains("cluck")) {
            System.out.println("Cluck!");
            theWorld.playSoundAtEntity(event.player, "mob.chicken.say", 1.0F, 1.0F);
            System.out.println("Tried to play a sound.");
        }
    }

    @SubscribeEvent
    public void onWorldEvent(WorldEvent.Load event) {
        if((event.world != null) && !event.world.isRemote)
            theWorld = event.world;
    }
}
