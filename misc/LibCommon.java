package bob.SaySounds.misc;

public class LibCommon {

    public static final String MOD_PREFIX = "SaySounds:";
    public static final String MOD_ID = "SaySounds";
    public static final String MOD_NAME = MOD_ID;
    // public static final String BUILD = "GRADLE:BUILD";
    // public static final String VERSION = "GRADLE:VERSION-" + BUILD;

    public static final String PROXY_COMMON = "bob.SaySounds.proxy.CommonProxy";
    public static final String PROXY_CLIENT = "bob.SaySounds.proxy.ClientProxy";

}
